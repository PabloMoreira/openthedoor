#include <Keypad.h>
#include <TaskScheduler.h>
#include <Wire.h>
#include <TimeLib.h>
#include <Arduino.h>
#include <DS1307RTC.h>
#include <MyBuffer.h>
#include <CheckAuth.h>
#include <DigiOut.h>

//#include <HIDSerial.h>

//HIDSerial Userial;


void print2digits(int number) {
  if (number >= 0 && number < 10) {
    Serial.write('0');
  }
  Serial.print(number);
}

const byte ROWS = 4; //four rows
const byte COLS = 3; //three columns
char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};
byte rowPins[ROWS] = {2, 3, 4, 5}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {6, 7, 8}; //connect to the column pinouts of the keypad

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );



void keyCallback();
void ledCallback();
void rtcCallback();
void serialCallback();
void openPortCallback();
void closePortCallback();

Task TaskKey(1, TASK_FOREVER, &keyCallback);
Task TaskLed(2000,TASK_FOREVER, &ledCallback);
//Task TaskRtc(3000,TASK_FOREVER, &rtcCallback);
Task TaskSerial(1,TASK_FOREVER, &serialCallback);
Task TaskOpenPort(3500,TASK_FOREVER,&openPortCallback);

bool state = false;
int incomingByte = 0;
Scheduler runner;
MyBuffer *_input = new MyBuffer(20);
MyBuffer *_input_key = new MyBuffer(10);
CheckAuth auth;
DigiOut RelayPort;

void setup(){
  Serial.begin(9600);
  //Userial.begin();
  //Userial.println("Hello World!");
  //Userial.poll();
  runner.init();
  runner.addTask(TaskKey);
  runner.addTask(TaskLed);
  //runner.addTask(TaskRtc);
  runner.addTask(TaskSerial);
  runner.addTask(TaskOpenPort);
  TaskKey.enable();
  TaskLed.enable();
  //TaskRtc.enable();
  TaskSerial.enable();

  RelayPort.setPin(A0);

  pinMode(A1, OUTPUT);

}

void loop(){
  runner.execute();
}

void keyCallback()
{
    char key = keypad.getKey();

    if (key != NO_KEY)
    {
    //Serial.println(key);
      _input_key->add(key);
      if (key == '#')
      {
        TaskKey.delay(1000);//Demora la entrada del teclado;
        _input_key->clean();
        //Serial.println((char*)_input_key->get());
        if (auth.check((char*)_input_key->get()))
        {
            TaskOpenPort.restart();
            TaskOpenPort.enable();
            Serial.print("Key Accept");
            Serial.print(" id-> ");
            Serial.println(auth.get_id());
            TaskLed.setInterval(125);
            rtcCallback();
        }
        else
        {
            TaskKey.delay(5000);//memora la entrada del teclado

            digitalWrite(A1,HIGH);
            TaskLed.delay(5000);
            Serial.println("Key Block");
        }
      }
    }
}

void ledCallback()
{
  state = !state;
  digitalWrite(A1,state);

}

void openPortCallback()
{
    RelayPort.on();
    TaskKey.disable();
    TaskOpenPort.setCallback(closePortCallback);
}

void closePortCallback()
{
    RelayPort.off();
    TaskOpenPort.setCallback(openPortCallback);
    TaskOpenPort.disable();
    TaskKey.enable();
    TaskLed.setInterval(2000);
}

void rtcCallback(){
  tmElements_t tm;

  if (RTC.read(tm)) {
    Serial.print("Ok, Time = ");
    print2digits(tm.Hour);
    Serial.write(':');
    print2digits(tm.Minute);
    Serial.write(':');
    print2digits(tm.Second);
    Serial.print(", Date (D/M/Y) = ");
    Serial.print(tm.Day);
    Serial.write('/');
    Serial.print(tm.Month);
    Serial.write('/');
    Serial.print(tmYearToCalendar(tm.Year));
    Serial.println();
  } else {
    if (RTC.chipPresent()) {
      Serial.println("The DS1307 is stopped.  Please run the SetTime");
      Serial.println("example to initialize the time and begin running.");
      Serial.println();
    } else {
      Serial.println("DS1307 read error!  Please check the circuitry.");
      Serial.println();
    }
    //delay(9000);
  }
  //delay(1000);
}

void serialCallback()
{
    int Year,Month,Day,Hour, Min, Sec;
    tmElements_t tm;

    //$ date +"%Y,%m,%d,%H,%M,%S" > /dev/ttyACM0

    //sscanf("" , "%d,%d,%d,%d,%d,%d" ,&Year,&Month,&Day,%Hour,%Min,%Sec);


    if (Serial.available() > 0)
    {
      if(_input->add(Serial.read()) == false)
      {
        if (sscanf((char*)_input->get(), "%d,%d,%d,%d,%d,%d", &Year,&Month,&Day,&Hour,&Min,&Sec) == 6)
        {
          //se podria limpiar el buffer
          tm.Day = Day;
          tm.Month = Month;
          tm.Year = CalendarYrToTm(Year);
          tm.Hour = Hour;
          tm.Minute = Min;
          tm.Second = Sec;
          RTC.write(tm);
        }
      }             //Serial.println(incomingByte, DEC);
  }

}
