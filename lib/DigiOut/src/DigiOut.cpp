#include <DigiOut.h>

DigiOut::DigiOut()
{};
void DigiOut::setPin(uint8_t pin)
{
    pinMode(pin,OUTPUT);
    this->_pin = pin;
};

void DigiOut::on()
{
    digitalWrite(this->_pin,HIGH);
};
void DigiOut::off()
{
    digitalWrite(this->_pin,LOW);
};
