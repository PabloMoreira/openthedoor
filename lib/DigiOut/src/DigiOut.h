#ifndef DigiOut_h
#define DigiOut_h
#include <Arduino.h>


class DigiOut
{
    public:
      DigiOut();
      void setPin(uint8_t pin);
      void on();
      void off();

    private:
        uint8_t _pin;
};
#endif
