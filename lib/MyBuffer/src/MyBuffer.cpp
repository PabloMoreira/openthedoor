
#include <MyBuffer.h>
MyBuffer::MyBuffer(uint8_t len)
{
    this->_len = len;
    this->_data = (uint8_t*)malloc(this->_len);
    this->_index = 0;
}

bool MyBuffer::add(uint8_t data)
{
  if(this->_index < this->_len - 1) //para dejar lugar al 0 de fin de string
  {
    this->_data[this->_index] = data;
    this->_index++;
    return true;
  }
  return false;
}

uint8_t* MyBuffer::get()
{
    return this->_data;
}

void MyBuffer::clean()
{
    this->_data[this->_index] = 0;
    this->_index = this->_len;
    this->_index = 0;
}
