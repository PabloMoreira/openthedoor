#ifndef MyBuffer_h
#define MyBuffer_h
#include <Arduino.h>

class MyBuffer
{
  public:
    MyBuffer(uint8_t len);
    bool add(uint8_t data);//buffer full return false
    uint8_t* get();
    void clean();

  private:
    uint8_t _index;
    uint8_t* _data;
    uint8_t _len;
};
#endif
