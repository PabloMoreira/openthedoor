#ifndef CheckAuth_h
#define CheckAuth_h
#include <Arduino.h>


class CheckAuth
{
  public:
    CheckAuth();
    bool check(char*);
    bool check(bool);
    int get_id();
  private:
    bool sd_compare(char*);
    int _id;
};
#endif
